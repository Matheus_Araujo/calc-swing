import java.awt.BorderLayout;

import javax.swing.JFrame;
import javax.swing.JPanel;

public class Calc1 extends JFrame {

	public Calc1() {
		super("Calculadora do passado");
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(300,350);
		setLocation(400, 200);
		
		setLayout(new BorderLayout());
		
		TextPanel textPanel = new TextPanel();
		add(BorderLayout.NORTH, textPanel);
		
		JPanel digitoPanel = new JPanel();
		digitoPanel.setLayout(new BorderLayout());
		digitoPanel.add(BorderLayout.CENTER,new NumbersPanel(textPanel.getTxtNumber()));
		digitoPanel.add(BorderLayout.EAST,new OperationsPanel(textPanel.getTxtNumber()));
		add(BorderLayout.CENTER, digitoPanel);
		
		
		
		
		
		setVisible(true);
		
	}
	public static void main(String[] args) throws Exception{
		new Calc1();
	}
}
